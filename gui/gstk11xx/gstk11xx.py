#! env python
# coding: utf-8

# GUI for Syntek USB video camera
#
# Copyright (C) Nicolas Albert <nicolas_albert_85@yahoo.fr>
#
# Version 0.0.1
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA


import pygtk
pygtk.require('2.0')
import gtk.glade
import gtk

class gladeAutoconnect:
    def __init__(self,glade,window):
      self.widget = gtk.glade.XML(glade, window)
      dico = {}
      self.dico_recurcif(self.__class__, dico)
      self.widget.signal_autoconnect(dico)

    def dico_recurcif (self, classe, dico):
        bases = classe.__bases__
        for iteration in bases:
            self.dico_recurcif(iteration, dico)
        for iteration in dir(classe):
            dico[iteration]=getattr(self,iteration)

class root(gladeAutoconnect):
  def __init__(self):
    gladeAutoconnect.__init__(self, "gstk11xx.glade", "root")
    self.refreshValues()

  def on_root_delete_event(self, source = None, event = None):
    gtk.main_quit()

  def on_toggle(self, source, event = None):
    self.apply(source.get_name(), str(int(source.get_active())))
    
  def on_scale(self, source):
    value = "%.4X"%source.get_value()
    name = source.get_name()
    self.widget.get_widget("val_"+name).set_label(value)
    self.apply(name, value)
    
  def apply(self, name, value):
    file = open("/sys/class/video4linux/video0/"+name,"w")
    file.write(value)
    file.close()
  
  def read(self, name):
    file = open("/sys/class/video4linux/video0/"+name,"r")
    res = file.read()[:-1]
    file.close()
    return res
  
  def refreshValues(self):
    for name in ["vflip","hflip","brightness","contrast","whitebalance","colour"]:
      value = self.read(name)
      wg = self.widget.get_widget(name)
      if isinstance(wg, gtk.CheckButton): wg.set_active(int(value))
      elif isinstance(wg, gtk.HScale): wg.set_value(int(value,16))

if __name__ ==  '__main__':
  win = root()
  gtk.main()
