#!/usr/bin/perl

###
# a helper for parsing the very long usb log files
# please note that some values are dc-1125 specific
#
# also, the code is not perfect or bullet proof 
# but for our case it should do 

my @lines;
my $count = 0;
my $framesize = 0;

sub IN_NOWHERE {0;}
sub IN_ISO {5;}
sub IN_CONTROL {7;}
sub IN_VENDOR_DEVICE {9;}
sub IN_VENDOR_INTERFACE {91;}
sub IN_SEL_INTERFACE {10;}
sub UN_SET_FEATURE {11;}
sub URB_SENDING {1;}
sub URB_GETTING {2;}
sub URB_NONE {0;}

my $state = IN_NOWHERE;
my $urbstate = URB_NONE;


## step over the file start
## configure the skip_lines accordingly to your file
$skip_lines = 0; #81*1000;
while($skip_lines-- > 0) {
	$count++;
	$dump = <>;
}
while(<>) {
	$count = $count + 1;
	$lst = "\@$count ";
	$lst = "\@ ";
	$line = $_;
	unshift (@lines, $line);
	if(scalar(@lines) > 10) {
		pop @lines;
	}
	if($line =~ /<<<  URB /) {
		#a call is returning
		$urbstate = URB_GETTING;
		$state = IN_NOWHERE;
	} elsif ($line =~ />>>  URB/  ) {
		# a call is being sent
		$urbstate = URB_SENDING;
		$state = IN_NOWHERE;
	} elsif ($line =~ /URB_FUNCTION_ISOCH_TRANSFER/) {
		$state = IN_ISO;
	} elsif ($line =~ /URB_FUNCTION_VENDOR_DEVICE/) {
		$state = IN_VENDOR_DEVICE;
	} elsif ($line =~ /URB_FUNCTION_VENDOR_INTERFACE/) {
		$state = IN_VENDOR_INTERFACE;
	} elsif ($line =~ /URB_FUNCTION_CONTROL_TRANSFER/) {
		$state = IN_CONTROL;
	} elsif ($line =~ /URB_FUNCTION_SELECT_INTERFACE/) {
		$state = IN_SEL_INTERFACE;
	} elsif ($line =~ /URB_FUNCTION_SET_FEATURE_TO_DEVICE/) {
		$state = IN_SET_FEATURE;
	} elsif ($line =~ /URB_/) {
		print "UNKNOWN $line\n";
	}

	if ($state == IN_NOWHERE) {
		if ($line =~ /\[[0-9]+ ms\]/) {
			$line =~ /\[([0-9]+) ms\]/;
			$time = $1;
		}
	}

	if($state == IN_CONTROL) {
		# a control return is returning, let's catch what it's carrying
		# previous line must be SetupPacket
		if($lines[1] =~ /SetupPacket/ ) {
			if (($line =~ /00000: 40/) || ($line =~ /00000: 41/)) {
				# setvalue confirmation
				print "$lst << SET " . $line;
			} else {
				# a getvalue is coming back
				$lines[3] =~ /0000: (..)/;
				$zzz = $1;
				print "$lst << GET {buf $zzz} " . $line;
			}
		}
	} elsif($state == IN_VENDOR_DEVICE) {
		if($line =~ /\s+Index\s+/) {
			$line = ~/00000(...)/;
			$indexstr = $1;
			$rline = $lines[2];
			$vline = $lines[1];
			$vline =~ /00000(...)/;
			$valuestr = $1;
			if($rline =~ /000001/) {
				print "$lst >> SET $indexstr = $valuestr \n";
			} else {
				print "$lst >> GET $indexstr = $valuestr \n";
			}
		}
	} elsif($state == IN_VENDOR_INTERFACE) {
		if($line =~ /\s+Index\s+/) {
			$line = ~/00000(...)/;
			$indexstr = $1;
			$rline = $lines[2];
			$vline = $lines[1];
			$vline =~ /00000(...)/;
			$valuestr = $1;
			if($rline =~ /000008/) {
				print "$lst >> SET $indexstr = $valuestr \n";
			} else {
				print "$lst >> GET $indexstr = $valuestr \n";
			}
		}
	} elsif($state == IN_ISO) {
		if($line =~ /PipeHandle/) {
			if($urbstate == URB_SENDING) {
				print "$lst >>> SENDING ISO REQUEST \n";
			} else {
				print "$lst <<< GETTING ISO RESPONSE \n";
			}
		}
	} elsif ($state == IN_SEL_INTERFACE) {
		if ($line =~ /AlternateSetting/) {
			$altintf =~ /([0-9]+)/;
			$value = $1;
			if($urbstate == URB_SENDING) {
				print "$lst >>> SENDING SELECT_INTERFACE $line\n";
			} else {
				print "$lst <<< GETTING SELECT_INTERFACE $line\n";
			}
		}
	} elsif ($state == IN_SET_FEATURE) {
		if ($line =~ /FeatureSelector/) {
			$value = $1;
			if($urbstate == URB_SENDING) {
				print "$lst >>> SENDING SET_FEATURE_TO_DEVICE $line\n";
			} else {
				print "$lst <<< GETTING SET_FEATURE_TO_DEVICE $line\n";
			}
		}
	}

	if (1 && ($state == IN_ISO)) {
		if ($urbstate == URB_GETTING) {
			if ($line =~ /\.Length = /){
				$line =~ / = ([0-9]+)/;
				$isolength = $1;
			}
			elsif ($line =~ /\.Status = /){
				if ($line =~ / = c[0-9]+/) {
					print "    URB ISOCH : Status = ERROR\n";
				}
				else {
				$line =~ / = ([0-9]+)/;
				$isostatus = $1;
	
				if ($isostatus == 0) {
					print "   URB ISOCH : Length = $isolength\n";

					if ($isolength > 8) {
						$framesize += ($isolength-4);
					}
					elsif ($isolength < 8) {
						if ($framesize > 0) {
							print "   SIZE OF FRAME = $framesize\n";
						}

						$framesize = 0;
					}
				}
				}
			}
		}
	}


}
