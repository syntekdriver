#!/usr/bin/perl

sub IN_NONE {0;}
sub IN_GET {1;}
sub IN_SET {2;}
sub IN_SELECT_CONFIGURATION {3;}
sub IN_SET_FEATURE {4;}

while (<>) {
	$line = $_;

	chomp $line;

	$line =~ s/^.*00[34]:00 [s|0] //g;

	$line =~ s/ [0-9]$//g;
	$line =~ s/ [0-9] <$//g;

	$start1 = substr($line, 0, 2);
	$start2 = substr($line, 3, 2);

	if ($start1 eq '40') {
		$mod = IN_SET;

		($p1, $p2, $p3, $p5, $p7) = split(/ /, $line);

		$p4 = substr($p3, 2, 2);
		$p3 = substr($p3, 0, 2);

		$p6 = substr($p5, 2, 2);
		$p5 = substr($p5, 0, 2);

		$p8 = substr($p7, 2, 2);
		$p7 = substr($p7, 0, 2);

		$save = "$p1 $p2 $p4 $p3 $p6 $p5 $p8 $p7";
	}
	elsif ($start1 eq 'c0') {
		$mod = IN_GET;
		
		($p1, $p2, $p3, $p5, $p7) = split(/ /, $line);

		$p4 = substr($p3, 2, 2);
		$p3 = substr($p3, 0, 2);

		$p6 = substr($p5, 2, 2);
		$p5 = substr($p5, 0, 2);

		$p8 = substr($p7, 2, 2);
		$p7 = substr($p7, 0, 2);

		$save = "$p1 $p2 $p4 $p3 $p6 $p5 $p8 $p7";
	}
	elsif ($start1 eq '00') {
		if ($start2  eq '01') {
			$mod = IN_SELECT_CONFIGURATION;
		}
		elsif ($start2 eq '03') {
			$mod = IN_SET_FEATURE;
		}

		($p1, $p2, $p3, $p5, $p7) = split(/ /, $line);

		$p4 = substr($p3, 2, 2);
		$p3 = substr($p3, 0, 2);

		$p6 = substr($p5, 2, 2);
		$p5 = substr($p5, 0, 2);

		$p8 = substr($p7, 2, 2);
		$p7 = substr($p7, 0, 2);

		$save = "$p1 $p2 $p4 $p3 $p6 $p5 $p8 $p7";
	}
	else {
		if ($mod == IN_GET) {
			($size, $result) = split(/ = /, $line);

			($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8) = split(/ /, $save);

			$reg = substr(($p6.$p5), 1, 3);
			$value = substr(($p4.$p3), 1, 3);

			$out = "@  >> GET $reg = $value \n";
			$out .= "@  << GET {buf $result}     00000000: $save\n";

			print $out;
		}
		elsif ($mod == IN_SET) {
			($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8) = split(/ /, $save);

			$reg = substr(($p6.$p5), 1, 3);
			$value = substr(($p4.$p3), 1, 3);

			$out = "@  >> SET $reg = $value \n";
			$out .= "@  << SET     00000000: $save\n";

			print $out;
		}
		elsif ($mod == IN_SELECT_CONFIGURATION) {
			($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8) = split(/ /, $save);

			$reg = substr(($p6.$p5), 1, 3);
			$value = substr(($p4.$p3), 1, 3);

			$out = "UNKNOWN -- URB_FUNCTION_SELECT_CONFIGURATION:\n\n";

			print $out;
		}
		elsif ($mod == IN_SET_FEATURE) {
			($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8) = split(/ /, $save);

			$reg = substr(($p6.$p5), 1, 3);
			$value = substr(($p4.$p3), 1, 3);

			$out = "@  >>> SENDING SET_FEATURE_TO_DEVICE   FeatureSelector = 0000$value \n\n";
			$out .= "@  << GET {buf }     00000000: $save\n";

			print $out;
		}
		
		
		$mod = IN_NONE;
	}
}

