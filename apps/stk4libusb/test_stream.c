/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <time.h>

#include "usbtools.h"
#include "stktools.h"


#define APPMODE_FULL 0
#define APPMODE_VIDEO_ONLY 1

int application_mode = 0;
FILE *output_f;


void exitm(char *message) {
	if(message != NULL) {
		printf("Exit: %s", message);
	}
	exit(1);
}

ut_usb_device *get_device() {
	return stk_find_device();
}

void log_hexdump(unsigned char *data, int len, int cols) {
	int i = 0;
	while(i < len) {
		fprintf(output_f , "%02x ", data[i]);
		i++;
		if(i%cols == 0) {
			fprintf(output_f, "\n");
		}
	}
	fprintf(output_f, "\n");
}

void handle_isoc_result(struct usbdevfs_urb *urb) {
	int i;
	int offset = 0;
	
	if(application_mode == APPMODE_FULL) {
		fprintf(output_f, "ISOC URB returning\n");
	}
	for(i=0;i<urb->number_of_packets;i++) {
		if(application_mode == APPMODE_FULL) { 
			fprintf(output_f, "Packet dump ActualLength:%d ReservedLength:%d \n", urb->iso_frame_desc[i].actual_length, urb->iso_frame_desc[i].length);
			log_hexdump(urb->buffer + offset, urb->iso_frame_desc[i].actual_length, 16);
		} else {
			int skip = 4;
			if(urb->iso_frame_desc[i].actual_length >= 4) {
				// we found something informational from there
				// the isoc frames have to type of headers
				// type1: 00 xx 00 00 or 20 xx 00 00
				// type2: 80 xx 00 00 00 00 00 00 or a0 xx 00 00 00 00 00 00
				// xx is a sequencer which has never been seen over 0x3f
				
				// imho data written down looks like bayer, i see similarities after 
				// every 640 bytes
				unsigned char *fbyte = urb->buffer + offset;
				if(*fbyte & 0x80) {
					skip = 8;
				}
				fwrite(urb->buffer + offset + skip, 1, urb->iso_frame_desc[i].actual_length - skip, output_f);
			}
			// less data than that we just ignore in cold blood
		}
		offset += urb->iso_frame_desc[i].length;
	}
	// ok
}

void try_video_loop(ut_usb_endpoint *ep) {
	int packets = 10; // 64 doesn't work, linux kernel limits us to less frames , what in the bloody ...
	int packetlength =  3 * 1024;
	int bufferLength = 2*packets * packetlength;
	int bufferCount = 4;
	int ret = 0;
	ut_usb_device *device;
	device = ep->interface->config->device;
	
	// prepare the buffers
	unsigned char **buffers;
	
	buffers = malloc(sizeof(unsigned char *) * bufferCount);
	memset(buffers, 0, sizeof(unsigned char *) * bufferCount);
	
	int i;
	for(i = 0; i < bufferCount; i++) {
		buffers[i] = malloc(bufferLength);
		if(buffers[i] == NULL) {
			// crap
			exitm("Failed to allocate memory buffers for isoc transfers");
		}
		memset(buffers[i], 0, bufferLength);
	}
	
	printf("Buffers are ready, time for action\n");
	
	// schedule reads
	for(i = 0; i < bufferCount; i++) {
		ut_submit_iso_read(ep, buffers[i], packetlength, packets, 0, NULL);
	}
	
	int resendLimit = 25000; // we'll do the following for 500 times if we can 
	int errorMet = 0; // if we see an error, just try to get out from here
	
	for(i=0; i< resendLimit && errorMet == 0; i++) {
		if( i%10 == 0 ) {
			subblock_omega(device); // the logfile indicate a read over some steps
		}
		if (i == 10) {
			subblock_alpha(device); // the logfiles indicate a "reconfiguration" after some isoc frames
		}
		struct usbdevfs_urb *urb;
		ret = ut_catch_iso_read(ep, &urb);
		
		if(ret >= 0 && urb != NULL) {
			handle_isoc_result(urb);
		}
		
		// don't ask for new buffers if we won't be here
		// to pick them up
		if( i < resendLimit - bufferCount ) {
			if(ret >=0 && urb != NULL) {
				printf("Resending urb in loop step %d \n", i);
				ret = ut_submit_iso_read(ep, urb->buffer, packetlength, packets, 0, urb);
				if(ret < 0) {
					printf("Resending iso read failed. Breaking out from loop\n");
					errorMet = 1;
				}
			} else {
				printf("Catch iso read definitely did not succeed, ret %d\n", ret);
				errorMet = 1;
			}
		}
		
	}
}

void stk_catch_stream(ut_usb_device *device) {
	int ret;
	ut_usb_config *config;
	ut_usb_interface *interface;
	ut_usb_interface *f_interface = NULL; // fast data interface
	ut_usb_interface *dummy_interface = NULL; // the first interface we meet
	ut_usb_endpoint *endpoint = NULL;
	endpoint = NULL; // make the compiler stfu
	ut_usb_endpoint *isoc_endpoint = NULL;
	
	
	/* open */
	printf("Opening device \n");
	ret = ut_open_device(device);
	if(ret != UT_OK) {
		exitm("Opening of the device failed");
	}
	
	/* we only have 1 config, don't split the hair here ... */
	config = device->configs;
	
	printf("Searching for high speed interface \n");
	/* find high speed interface */
	for(interface = config->interfaces; interface != NULL; interface = interface->next) {
		if(dummy_interface == NULL) {
			dummy_interface = interface;
		}
		if(interface->endpoints->next->descriptor->wMaxPacketSize == 0x1400) {
			printf("Found fast interface\n");
			// we found it 
			f_interface = interface;
			isoc_endpoint = f_interface->endpoints->next;
			break;
		} else {
			printf("Found interface with speed %04x, ignoring \n", 
				(int) interface->endpoints->next->descriptor->wMaxPacketSize);
		}
	}
	if(f_interface == NULL) {
		exitm("The high speed (0x1400) interface was not found, bailing out\n");
		return; // actually we never reach this point ...
	}
	


//	stk_get_info(device);


	/* set config */
	printf("Setting config \n");
	ret = ut_set_configuration(config);
	if(ret != UT_OK) {
		exitm("Set config failed");
	}
	
	
	/* claim , init */
	printf("Claiming interface \n");
	ret = ut_claim_interface(f_interface);
	if(ret != UT_OK) {
		exitm("Claim interface failed");
	}

	
	stk_initialization_first1(device);	

	stk_set_feature(device, 0);

	stk_initialization_first2(device);	

	// select "dummy_interface"
	ut_set_interface(dummy_interface);

	stk_initialization1(device);	
	
	// select fast altsetting
	ut_set_interface(f_interface);

	stk_initialization2(device);	

	// at first reset ep
	ut_reset_endpoint(isoc_endpoint);
	
	// iso handling , grin 
	try_video_loop(isoc_endpoint);
	
	
	// inside streaming ops, this happens,
	
	ret = stk_read_registry(device, 0x0001);  // 0005 @ 164056
	printf("Read %02x from 0x0001, we don't know what this means :D \n", ret);
	
	
	// this was done at least once after capture ... cancelling streams or smth ?
	ret = stk_read_registry(device, 0x0100); // 1530557
	stk_deadly_write_reg(device, 0x0100, 0x0021); 
	
	ret = stk_read_registry(device, 0x0104);  // 0005 @ 1530584
	printf("Read %02x from 0x0104, we don't know what this means :D \n", ret);
	ret = stk_read_registry(device, 0x0105);  // 0005 @ 164056
	printf("Read %02x from 0x0105, we don't know what this means :D \n", ret);
	ret = stk_read_registry(device, 0x0106);  // 0005 @ 164056
	printf("Read %02x from 0x0106, we don't know what this means :D \n", ret);
	
	stk_deadly_write_reg(device, 0x0100, 0x0021); 
	stk_deadly_write_reg(device, 0x0116, 0x0000); 
	stk_deadly_write_reg(device, 0x0117, 0x0000); 
	stk_deadly_write_reg(device, 0x0018, 0x0000); 
	
	ret = stk_read_registry(device, 0x0000);  // 0005 @ 164056
	stk_deadly_write_reg(device, 0x0000, 0x004C); 
	
	// select back to "dummy_interface"
	ut_set_interface(dummy_interface);
	
	/* release */
	printf("Releasing interface \n");
	ret = ut_release_interface(f_interface);
	if(ret != UT_OK) {
		exitm("Release interface failed");
	}
	
	/* close */
	printf("Closing device \n");
	ret = ut_close_device(device);
	if(ret != UT_OK) {
		exitm("Closing of the device failed");
	}
	
}

int main(int argc, char **argv) {
	ut_usb_device *dev;
	char logfile[1024];
	

	if( argc > 1 ) {
		if( strcmp("video", argv[1]) == 0) {
			printf("Video mode, only raw video data will be saved\n");
			application_mode = APPMODE_VIDEO_ONLY;
		}
	}
	
	
	sprintf(logfile, "logfile_%d.%s", (int) time(NULL) , application_mode == APPMODE_VIDEO_ONLY ? "data" : "txt");
	printf("Using logfile '%s' \n", logfile);
	
	
	output_f = fopen(logfile, "w");

	printf("Finding/grabbing device \n");
	dev = get_device();
	
	if(dev == NULL) {
		exitm("Device was not found\n");
	}
	
	stk_catch_stream(dev);
	
	printf("Application Done \n");
	
	fclose(output_f);

	return 0;
}

