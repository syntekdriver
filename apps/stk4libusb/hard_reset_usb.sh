#!/bin/bash

# hardcore for resetting the ehci usb devices, needed sometimes
# execute as root or with sudo

echo "resetting ehci";
rmmod ehci_hcd;
echo "sleeping 1 second before enableing again";
sleep 1;
modprobe ehci_hcd;
echo "done"
