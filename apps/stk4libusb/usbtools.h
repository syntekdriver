/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */

#ifndef USBTOOLS_H
#define USBTOOLS_H

#define UT_ERROR -1
#define UT_OK 0




/**
 * Our mini library to give us proper usb access. Libusb doesn't cut it yet
 *
 */

#define UT_USB_PATH "/proc/bus/usb"
 

#include <linux/usb.h> // some of our stuff comes from there
#include <linux/usbdevice_fs.h>
#include <stdint.h>

// in order to avoid collisions, we prefix everything with ut_

struct ut_usb_bus_s;
struct ut_usb_device_s;
struct ut_usb_config_s;
struct ut_usb_interface_s;
struct ut_usb_endpoint_s;

typedef struct ut_usb_bus_s ut_usb_bus;
typedef struct ut_usb_device_s ut_usb_device;

typedef struct ut_usb_config_s ut_usb_config;
typedef struct ut_usb_interface_s ut_usb_interface;

typedef struct ut_usb_endpoint_s ut_usb_endpoint;



struct ut_usb_bus_s {
	// ??? uint32_t location;  //it shows the bus id of the bus for libusb, lets ignore it for now
	char dirname[1024];
	ut_usb_device *devices;
	ut_usb_bus *next;
};

struct ut_usb_device_s {
	uint32_t devnum;
	char filename[1024];
	int fd;
	ut_usb_bus *bus;
	ut_usb_device *next;
	struct usb_device_descriptor *descriptor;
	ut_usb_config *configs;
};

struct ut_usb_config_s {
	struct usb_config_descriptor *descriptor;
	ut_usb_interface *interfaces;
	ut_usb_config *next;
	ut_usb_device *device;
};

struct ut_usb_interface_s {
	struct usb_interface_descriptor *descriptor;
	ut_usb_endpoint *endpoints;
	ut_usb_interface *next;
	ut_usb_config *config;
};

struct ut_usb_endpoint_s {
	struct usb_endpoint_descriptor *descriptor;
	ut_usb_endpoint *next;
	ut_usb_interface *interface;
};

/**
 * Returns the list of all buses and devices that we can open
 */
int ut_get_devices (ut_usb_bus **bus);

/**
 * Open device
 */
int ut_open_device(ut_usb_device *device);

/**
 * Close device
 */
int ut_close_device(ut_usb_device *device);

/**
 * Set the configuration 
 */
int ut_set_configuration(ut_usb_config *config);

/**
 * Claim the interface, the device is found out 
 * accordingly to the interface
 */
int ut_claim_interface(ut_usb_interface *interface);

/**
 * Claim the interface, the device is found out 
 * accordingly to the interface
 */
int ut_release_interface(ut_usb_interface *interface);

int ut_set_interface(ut_usb_interface *interface);

/**
 * usb 'standard' funcs 
 */
int ut_control_msg(
	ut_usb_device *dev, int requesttype, int request, 
	int value, int index, char *bytes, int size, int timeout
);


int ut_usb_bulk();

int ut_reset_device(ut_usb_device *dev);

int ut_reset_endpoint(ut_usb_endpoint *dev);

int ut_submit_iso_read(ut_usb_endpoint *ep, unsigned char *buffer, 
	int packet_size, int packet_count, int error_signal, struct usbdevfs_urb *template);

int ut_catch_iso_read(ut_usb_endpoint *ep, struct usbdevfs_urb **ref);

void ut_hexdump(unsigned char *data, int length, int cols);

#endif // USBTOOLS_H

