/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <errno.h>


#include <linux/usb.h>
#include <linux/usbdevice_fs.h>

#include "stktools.h"
#include "usbtools.h"



#define _GNU_SOURCE

// comment the following line to disable debugging
#define DEBUG 1

#ifdef DEBUG
#define SDEBUG(x) printf("[SDEBUG] "); x ; printf("\n");
#else 
#define SDEBUG(x)
#endif /*DEBUG*/


ut_usb_device *stk_find_device() {
	ut_usb_bus *bus;
	ut_usb_device *device;
	int i, device_count;
	
	device_count = ut_get_devices(&bus);
	
	if(device_count < 1) {
		printf("Could not find any usb items on the system, make sure you are root \n");
		return NULL;
	}
	
	i=0;
	for(;bus != NULL;bus = bus->next) {
		for(device = bus->devices;device != NULL;device = device->next) {
			SDEBUG( printf("Device %d '%s' \n", i++, device->filename); );
			if(device->descriptor->idVendor == SYNTEK_VENDOR_ID
				&& device->descriptor->idProduct == SYNTEK_DC1125_ID) {
				printf("Found dc1125\n");	
				return device;
			} else {
				SDEBUG( 
					printf("Found device %04x:%04x \n",
						device->descriptor->idVendor ,
						device->descriptor->idProduct);
				);
			}
		}
	}
	
	return NULL;
}

#define XTIMEOUT 1000


/* GET INFO */
int stk_get_info(ut_usb_device *device) {
	int i;
	int ret;
	char buf[1000];
	
	ret = ut_control_msg(
			device, USB_RECIP_DEVICE | USB_DIR_IN, USB_REQ_GET_DESCRIPTOR,
		     1, 0,
			 buf, 0x12, XTIMEOUT);

	for (i=0; i<0x12; i++) 
		printf("%02X ", buf[i]);

	printf("\n");

	ret = ut_control_msg(
			device, USB_RECIP_DEVICE | USB_DIR_IN, USB_REQ_GET_DESCRIPTOR,
		     2, 0,
			 buf, 0x63, XTIMEOUT);

	ret = ut_control_msg(
			device, USB_RECIP_DEVICE | USB_DIR_IN, USB_REQ_GET_DESCRIPTOR,
		     2, 0,
			 buf, 0x93, XTIMEOUT);

	return ret;
}


/* SET_FEATURE */
int stk_set_feature(ut_usb_device *device, int index) {
	int ret;

	ret = ut_control_msg(
		device, USB_RECIP_DEVICE,  USB_REQ_SET_FEATURE,
			USB_DEVICE_REMOTE_WAKEUP, index,
			NULL, 0, XTIMEOUT
			);


	if(ret < 0) {
		printf("ut_control_msg returned ret < 0");
		perror("let's see what perror tells us ...");
		return ret;
	}

	return ret;
}

/* writes a value to the device registry */
/* returns UT_ERROR on error */
int stk_write_registry(ut_usb_device *device, int index, int value) {
	int ret;
	ret = ut_control_msg(
		device, USB_TYPE_VENDOR | USB_DIR_OUT | USB_RECIP_DEVICE, 0x01,
		value, index, NULL, 0, XTIMEOUT
	);
	if(ret < 0) {
		printf("ut_control_msg returned ret < 0");
		perror("let's see what perror tells us ...");
		return ret;
	}
	return ret;
}

/* reads a value from the device registry */
/* return UT_ERROR on error */
int stk_read_registry(ut_usb_device *device, int index) {
	int ret;
	uint8_t value = 0x00;
	ret = ut_control_msg(
		device, USB_TYPE_VENDOR | USB_DIR_IN | USB_RECIP_DEVICE, 0x00,
		value, index, (char *) &value, sizeof(uint8_t), XTIMEOUT
	);

	if(ret < 0) {
		printf("ut_control_msg returned ret < 0 \n");
		perror("let's see what perror tells us ...");
		return UT_ERROR; //ret;
	}
	return value;
}

int stk_reset_device(ut_usb_device *dev) {
	int ret;
	/* open */
	printf("Opening device \n");
	ret = ut_open_device(dev);
	if(ret != UT_OK) {
		printf("Opening of the device failed");
		return UT_ERROR;
	}
	
	/* reset the device */
	ret = ut_reset_device(dev);
	if(ret == UT_OK) {
		printf("Reset succeeded \n");
	} else {
		printf("Reset failed \n");
		return UT_ERROR;
	}
	
	/* close */
	printf("Closing device \n");
	ret = ut_close_device(dev);
	if(ret != UT_OK) {
		printf("Closing of the device failed");
		return UT_ERROR;
	}
	return 0;
}

int stk_deadly_write_reg(ut_usb_device *device, int index, int value) {
	int ret;
	ret = stk_write_registry(device, index, value);
	if(ret == UT_ERROR) {
		perror("Registry write failed");
		exit(-1);
	}
	return ret;
}



int stk_initialization_first1(ut_usb_device *device) {
	int ret = 0;
	int i = 0; 
	int j = 0;
	int response_ok = 0;
	
	printf("Processing init - first step\n");

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006F);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006D);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);

	for (i=0; i<16; i++) {
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_read_registry(device, 0x0000);
	}

	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006F);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006D);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);

	for (i=0; i<16; i++) {
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_read_registry(device, 0x0000);
	}

	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006F);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0027);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0026);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006D);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);

	for (i=0; i<16; i++) {
	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_read_registry(device, 0x0000);
	}

	ret = stk_deadly_write_reg(device, 0x0000, 0x0025);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020);
	ret = stk_deadly_write_reg(device, 0x0002, 0x006F);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020);

	ret = stk_deadly_write_reg(device, 0x0117, 0x0000);
	ret = stk_read_registry(device, 0x0103);
	ret = stk_deadly_write_reg(device, 0x0103, 0x0001);
	ret = stk_read_registry(device, 0x0103);
	ret = stk_deadly_write_reg(device, 0x0103, 0x0000);

	ret = stk_deadly_write_reg(device, 0x0000, 0x00E0);
	ret = stk_deadly_write_reg(device, 0x0002, 0x00E8);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020);


	// conf0

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000e);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0006);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001e);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0007);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0007);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0087);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e7);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0020); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0040); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0041); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0001); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x001C); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0002); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<64;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_deadly_write_reg(device, 0x0200, 0x0008); 

	// conf1

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x0003);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x000A);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0022); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0027); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00A5); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<64;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_deadly_write_reg(device, 0x0200, 0x0008); 

	//conf2

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00BF); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0208, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000A); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000B); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 


	//---------------
	// conf3

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0042); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0024); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00A5); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<64;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_deadly_write_reg(device, 0x0200, 0x0008); 


	//---------------
	// conf4

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0042); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00E0); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0024); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00A5); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<64;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_deadly_write_reg(device, 0x0200, 0x0008); 

	//---------------
	// conf5

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x003E);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0009);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x00BE);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e9);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00FF); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0208, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000A); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000B); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	//---------------
	// conf6

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x003E);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0009);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x00BE);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e9);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00FF); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0208, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000A); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000B); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 


	//---------------
	// conf7

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0009);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0012); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0204, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0205, 0x00B7); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0005); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}


	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0208, 0x0013); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000A); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 

	ret = stk_deadly_write_reg(device, 0x0208, 0x000B); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0020); 

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x01) {
			printf("\n 0x01 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}

	ret = stk_read_registry(device, 0x0209);

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 


	//---------------
	// conf8

	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 


	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 


	//----------------------

	int values_204[] = {
		0x12, 0x11, 0x3b, 0x6a, 0x13, 0x10, 0x00, 0x01, 0x02, 0x13,
		0x39, 0x38, 0x37, 0x35, 0x0e, 0x12, 0x04, 0x0c, 0x0d, 0x17,
		0x18, 0x32, 0x19, 0x1a, 0x03, 0x1b, 0x16, 0x33, 0x34, 0x41,
		0x96, 0x3d, 0x69, 0x3a, 0x8e, 0x3c, 0x8f, 0x8b, 0x8c, 0x94,
		0x95, 0x40, 0x29, 0x0f, 0xa5, 0x1e, 0xa9, 0xaa, 0xab, 0x90,
		0x91, 0x9f, 0xa0, 0x24, 0x25, 0x26, 0x14, 0x2a, 0x2b 
	};
	int values_205[] = {
		0x45, 0x80, 0x01, 0x7d, 0x80, 0x00, 0x00, 0x80, 0x80, 0x80,
		0x50, 0x93, 0x00, 0x81, 0x20, 0x45, 0x00, 0x00, 0x00, 0x24,
		0xc4, 0xb6, 0x00, 0x3c, 0x36, 0x00, 0x07, 0xe2, 0xbf, 0x00,
		0x04, 0x19, 0x40, 0x0d, 0x00, 0x73, 0xdf, 0x06, 0x20, 0x88,
		0x88, 0xc1, 0x3f, 0x42, 0x80, 0x04, 0xb8, 0x92, 0x0a, 0x00,
		0x00, 0x00, 0x00, 0x68, 0x5c, 0xc3, 0x2e, 0x00, 0x00
	};
	
	
	for(j=0; j < 59; j++) {
		ret = stk_read_registry(device, 0x02FF);
		stk_deadly_write_reg(device, 0x02FF, 0x0);

		ret = stk_deadly_write_reg(device, 0x0204, values_204[j]);
		ret = stk_deadly_write_reg(device, 0x0205, values_205[j]);
		ret = stk_deadly_write_reg(device, 0x0200, 0x0001);
		
		/** 
		 * TODO : 
		 * after this , the hunt for 201 begins, the logs
		 * go separate ways here. it asked for multiple times
		 * and at some point one log show the windows driver
		 * giving up, and the whole init is started from scratch.
		 */
		response_ok = 0;
		for(i=0;i<500;i++) {
			ret = stk_read_registry(device, 0x0201);
			if(ret == 0) {
				printf(".");
				//fsync(stdout);
			} else if(ret == 0x04) {
				printf("\n 0x04 read from 201, we're fine\n");
				response_ok = 1;
				break;
			} else {
				printf("\nRead %02x from 201, we don't know what this means \n", ret);
			}
		}
		if(!response_ok) {
			printf("Failed to read 0x04 from 201, bailing out \n");
			printf("Failed loop step %d, values %02x and %02x \n", j, values_204[j], values_205[j] );
			exit(-1);
		}
		
		stk_deadly_write_reg(device, 0x02FF, 0x0);
	}
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	//-----------------


	stk_deadly_write_reg(device, 0x0200, 0x0080);
	stk_deadly_write_reg(device, 0x0200, 0x0000);
	stk_deadly_write_reg(device, 0x02FF, 0x0001);
	stk_deadly_write_reg(device, 0x0203, 0x00A0);

	ret = stk_read_registry(device, 0x0104);
	ret = stk_read_registry(device, 0x0105);
	ret = stk_read_registry(device, 0x0106);

	stk_deadly_write_reg(device, 0x0100, 0x0021);
	stk_deadly_write_reg(device, 0x0116, 0x0000);
	stk_deadly_write_reg(device, 0x0117, 0x0000);
	stk_deadly_write_reg(device, 0x0118, 0x0000);
	stk_deadly_write_reg(device, 0x0018, 0x0000);

	ret = stk_read_registry(device, 0x0000);
	stk_deadly_write_reg(device, 0x0000, 0x004C);

	return 0;
}



int stk_initialization_first2(ut_usb_device *device) {
	int ret = 0;
	int i = 0; 
	int j = 0;
	int response_ok = 0;
	
	printf("Processing init - second step\n");


	ret = stk_deadly_write_reg(device, 0x0000, 0x00E0);
	ret = stk_deadly_write_reg(device, 0x0002, 0x00E8);
	ret = stk_deadly_write_reg(device, 0x0000, 0x0020); 

	// conf9
	
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000E);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001E);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001);

	ret = stk_read_registry(device, 0x0100);
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 

	ret = stk_deadly_write_reg(device, 0x0200, 0x0080); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000); 


	ret = stk_deadly_write_reg(device, 0x0203, 0x0060); 


	//----------------------

	int values_204[] = {
		0x12, 0x11, 0x3b, 0x6a, 0x13, 0x10, 0x00, 0x01, 0x02, 0x13,
		0x39, 0x38, 0x37, 0x35, 0x0e, 0x12, 0x04, 0x0c, 0x0d, 0x17,
		0x18, 0x32, 0x19, 0x1a, 0x03, 0x1b, 0x16, 0x33, 0x34, 0x41,
		0x96, 0x3d, 0x69, 0x3a, 0x8e, 0x3c, 0x8f, 0x8b, 0x8c, 0x94,
		0x95, 0x40, 0x29, 0x0f, 0xa5, 0x1e, 0xa9, 0xaa, 0xab, 0x90,
		0x91, 0x9f, 0xa0, 0x24, 0x25, 0x26, 0x14, 0x2a, 0x2b 
	};
	int values_205[] = {
		0x45, 0x80, 0x01, 0x7d, 0x80, 0x00, 0x00, 0x80, 0x80, 0x80,
		0x50, 0x93, 0x00, 0x81, 0x20, 0x45, 0x00, 0x00, 0x00, 0x24,
		0xc4, 0xb6, 0x00, 0x3c, 0x36, 0x00, 0x07, 0xe2, 0xbf, 0x00,
		0x04, 0x19, 0x40, 0x0d, 0x00, 0x73, 0xdf, 0x06, 0x20, 0x88,
		0x88, 0xc1, 0x3f, 0x42, 0x80, 0x04, 0xb8, 0x92, 0x0a, 0x00,
		0x00, 0x00, 0x00, 0x68, 0x5c, 0xc3, 0x2e, 0x00, 0x00
	};
	
	
	for(j=0; j < 59; j++) {
		ret = stk_read_registry(device, 0x02FF);
		stk_deadly_write_reg(device, 0x02FF, 0x0);

		ret = stk_deadly_write_reg(device, 0x0204, values_204[j]);
		ret = stk_deadly_write_reg(device, 0x0205, values_205[j]);
		ret = stk_deadly_write_reg(device, 0x0200, 0x0001);
		
		/** 
		 * TODO : 
		 * after this , the hunt for 201 begins, the logs
		 * go separate ways here. it asked for multiple times
		 * and at some point one log show the windows driver
		 * giving up, and the whole init is started from scratch.
		 */
		response_ok = 0;
		for(i=0;i<500;i++) {
			ret = stk_read_registry(device, 0x0201);
			if(ret == 0) {
				printf(".");
				//fsync(stdout);
			} else if(ret == 0x04) {
				printf("\n 0x04 read from 201, we're fine\n");
				response_ok = 1;
				break;
			} else {
				printf("\nRead %02x from 201, we don't know what this means \n", ret);
			}
		}
		if(!response_ok) {
			printf("Failed to read 0x04 from 201, bailing out \n");
			printf("Failed loop step %d, values %02x and %02x \n", j, values_204[j], values_205[j] );
			exit(-1);
		}
		
		stk_deadly_write_reg(device, 0x02FF, 0x0);
	}
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	ret = stk_deadly_write_reg(device, 0x0104, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0105, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0106, 0x0000); 

	return 0;
}



int stk_initialization1(ut_usb_device *device) {
	int ret = 0;
	int i = 0; 
	int j = 0;
	int response_ok = 0;
	
	printf("Processing initialization 1 sequence (we have no idea what exactly will follow) \n");

	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0204, 0x002a);
	ret = stk_deadly_write_reg(device, 0x0205, 0x0000); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0001); 
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);

	ret = stk_deadly_write_reg(device, 0x0204, 0x002b);
	ret = stk_deadly_write_reg(device, 0x0205, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0200, 0x0001);

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);





	
	stk_deadly_write_reg(device, 0x0200, 0x0000); 
	// stuff at 93767
	int postvalues[] = {
//		0xa1, 0x00, 0x10, 0x7c, 0x04, 0x02, 0x00, 0x02, 0x2e, 0x00, 0x2d, 0xfc
		0xa1, 0x00, 0x10, 0x7c, 0x04, 0x02, 0x00, 0x02, 0x2e, 0x03, 0x2d, 0xea
	};
	
	for(j=0; j < 12; j+=2) {
		ret = stk_deadly_write_reg(device, 0x0204, postvalues[j]);
		ret = stk_deadly_write_reg(device, 0x0205, postvalues[j+1]);
	}
	// strange commit, but then again, previous writes were strange too
	stk_deadly_write_reg(device, 0x0200, 0x0006); 
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
			//fsync(stdout);
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("\nWe read zeroes similarly to logfile, this could be normal \n");
	}


	return 0;
}

int stk_initialization2(ut_usb_device *device) {
	int ret = 0;
	int i = 0; 
	int j = 0;
	int response_ok = 0;
	
	printf("Processing initialization 2 sequence (we have no idea what exactly will follow) \n");

	// this is exactly what we read from the usb log
	ret = stk_deadly_write_reg(device, 0x0000, 0x0024);
	ret = stk_deadly_write_reg(device, 0x0002, 0x0068);
	ret = stk_deadly_write_reg(device, 0x0003, 0x0080);
	ret = stk_deadly_write_reg(device, 0x0005, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x0007, 0x0003);
	ret = stk_deadly_write_reg(device, 0x000d, 0x0000);
	ret = stk_deadly_write_reg(device, 0x000f, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0012);
	ret = stk_deadly_write_reg(device, 0x0350, 0x0041);
	
	ret = stk_deadly_write_reg(device, 0x0351, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0352, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0353, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0018, 0x0010);
	ret = stk_deadly_write_reg(device, 0x0019, 0x0000);
	
	ret = stk_deadly_write_reg(device, 0x001b, 0x000e);	
	ret = stk_deadly_write_reg(device, 0x001c, 0x0046);
	ret = stk_deadly_write_reg(device, 0x0300, 0x0080);
	ret = stk_deadly_write_reg(device, 0x001a, 0x0004);
	ret = stk_deadly_write_reg(device, 0x0202, 0x001e);
	
	ret = stk_deadly_write_reg(device, 0x0110, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0111, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0112, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0113, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0114, 0x0080);
	
	ret = stk_deadly_write_reg(device, 0x0115, 0x0002);
	ret = stk_deadly_write_reg(device, 0x0116, 0x00e0);
	ret = stk_deadly_write_reg(device, 0x0117, 0x0001); /* line 82421 in usb log 0005 */
	
	// in examples this returns 0x00 or 0x21
	ret = stk_read_registry(device, 0x0100);
	printf("Registry read returned %02x \n", ret);
	
	// nvm what the previous returns, this is set to 21 in usb logs
	ret = stk_deadly_write_reg(device, 0x0100, 0x0021); 


	stk_deadly_write_reg(device, 0x0200, 0x0080);
	stk_deadly_write_reg(device, 0x0200, 0x0000);
	stk_deadly_write_reg(device, 0x02FF, 0x0000);
	stk_deadly_write_reg(device, 0x0203, 0x0060);

	
	/**
	 * What follows here is quite interesting ...
	 * registries 204 and 205 are written multiple times.
	 * each time followed by 0x0200 = 0x0005. I do believe 
	 * that this is some kind of probing code. for shortness
	 * of code, we have looped this
	 */
	
	// after writing all this down ... is this some kind
	// of drop in replacement of firmware ? cause it's
	// way too small ...
	
	int values_204[] = {
		0x12, 0x11, 0x3b, 0x6a, 0x13, 0x10, 0x00, 0x01, 0x02, 0x13,
		0x39, 0x38, 0x37, 0x35, 0x0e, 0x12, 0x04, 0x0c, 0x0d, 0x17,
		0x18, 0x32, 0x19, 0x1a, 0x03, 0x1b, 0x16, 0x33, 0x34, 0x41,
		0x96, 0x3d, 0x69, 0x3a, 0x8e, 0x3c, 0x8f, 0x8b, 0x8c, 0x94,
		0x95, 0x40, 0x29, 0x0f, 0xa5, 0x1e, 0xa9, 0xaa, 0xab, 0x90,
		0x91, 0x9f, 0xa0, 0x24, 0x25, 0x26, 0x14, 0x2a, 0x2b 
	};
	int values_205[] = {
		0x45, 0x80, 0x01, 0x7d, 0x80, 0x00, 0x00, 0x80, 0x80, 0x80,
		0x50, 0x93, 0x00, 0x81, 0x20, 0x45, 0x00, 0x00, 0x00, 0x24,
		0xc4, 0xb6, 0x00, 0x3c, 0x36, 0x00, 0x07, 0xe2, 0xbf, 0x00,
		0x04, 0x19, 0x40, 0x0d, 0x00, 0x73, 0xdf, 0x06, 0x20, 0x88,
		0x88, 0xc1, 0x3f, 0x42, 0x80, 0x04, 0xb8, 0x92, 0x0a, 0x00,
		0x00, 0x00, 0x00, 0x68, 0x5c, 0xc3, 0x2e, 0x10, 0x30
	};
	
	
	for(j=0; j < 59; j++) {
		ret = stk_read_registry(device, 0x02FF);
		stk_deadly_write_reg(device, 0x02FF, 0x0);

		ret = stk_deadly_write_reg(device, 0x0204, values_204[j]);
		ret = stk_deadly_write_reg(device, 0x0205, values_205[j]);
		ret = stk_deadly_write_reg(device, 0x0200, 0x0001);
		
		/** 
		 * TODO : 
		 * after this , the hunt for 201 begins, the logs
		 * go separate ways here. it asked for multiple times
		 * and at some point one log show the windows driver
		 * giving up, and the whole init is started from scratch.
		 */
		response_ok = 0;
		for(i=0;i<500;i++) {
			ret = stk_read_registry(device, 0x0201);
			if(ret == 0) {
				printf(".");
				//fsync(stdout);
			} else if(ret == 0x04) {
				printf("\n 0x04 read from 201, we're fine\n");
				response_ok = 1;
				break;
			} else {
				printf("\nRead %02x from 201, we don't know what this means \n", ret);
			}
		}
		if(!response_ok) {
			printf("Failed to read 0x04 from 201, bailing out \n");
			printf("Failed loop step %d, values %02x and %02x \n", j, values_204[j], values_205[j] );
			exit(-1);
		}
		
		stk_deadly_write_reg(device, 0x02FF, 0x0);
	}
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}


	ret = stk_deadly_write_reg(device, 0x0106, 0x0000);


	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);
	ret = stk_deadly_write_reg(device, 0x0204, 0x002a);
	ret = stk_deadly_write_reg(device, 0x0205, 0x0010); 
	ret = stk_deadly_write_reg(device, 0x0200, 0x0001); 
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);
	ret = stk_read_registry(device, 0x02FF);
	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);

	ret = stk_deadly_write_reg(device, 0x0204, 0x002b);
	ret = stk_deadly_write_reg(device, 0x0205, 0x0030);
	ret = stk_deadly_write_reg(device, 0x0200, 0x0001);

	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("Failed post loop step \n");
		printf("Failed to read 0x04 from 201, bailing out \n");
		exit(-1);
	}

	ret = stk_deadly_write_reg(device, 0x02FF, 0x0000);





	
	stk_deadly_write_reg(device, 0x0200, 0x0000); 
	// stuff at 93767
	int postvalues[] = {
		0xa1, 0x00, 0x10, 0x7c, 0x04, 0x02, 0x00, 0x02, 0x2e, 0x00, 0x2d, 0xfc
//		0xa1, 0x00, 0x10, 0x45, 0x04, 0x02, 0x00, 0x02, 0x2e, 0x00, 0x2d, 0x65
	};
	
	for(j=0; j < 12; j+=2) {
		ret = stk_deadly_write_reg(device, 0x0204, postvalues[j]);
		ret = stk_deadly_write_reg(device, 0x0205, postvalues[j+1]);
	}
	// strange commit, but then again, previous writes were strange too
	stk_deadly_write_reg(device, 0x0200, 0x0006); 
	
	response_ok = 0;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
			//fsync(stdout);
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("\nWe read zeroes similarly to logfile, this could be normal \n");
	}



	// 0005 , @106680 
	ret = stk_read_registry(device, 0x0116); 
	printf("Read %02x from 0x0116, we don't know what this means :D \n", ret);
	ret = stk_read_registry(device, 0x0117); 
	printf("Read %02x from 0x0117, we don't know what this means :D \n", ret);
	
	stk_deadly_write_reg(device, 0x0116, 0x0000); 
	stk_deadly_write_reg(device, 0x0117, 0x0000); 
	
	ret = stk_read_registry(device, 0x0100); 
	printf("Read %02x from 0x0100, we don't know what this means :D \n", ret);
	
	stk_deadly_write_reg(device, 0x0100, 0x00a1); 
	stk_deadly_write_reg(device, 0x0116, 0x00e0); 
	stk_deadly_write_reg(device, 0x0117, 0x0001); 
	
	// and here starts the video stream in windows usb log
	return 0;
}

int subblock_omega(ut_usb_device *device) {
	int ret;
	ret = stk_read_registry(device, 0x0001); /* log 0005, line 93359*/
	printf("Read %02x from 0x0001, we don't know what this means \n", ret);
	return ret;
}

int subblock_alpha(ut_usb_device *device) {
	int j;
	int ret;
	stk_deadly_write_reg(device, 0x0200, 0x0000); 
	// stuff at 181969 
	int postvalues[] = {
		0xa1, 0x00, 0x10, 0x7c, 0x04, 0x02, 0x00, 0x00, 0x2e, 0x03, 0x2d, 0xea
	};
	
	for(j=0; j < 12; j+=2) {
		ret = stk_deadly_write_reg(device, 0x0204, postvalues[j]);
		ret = stk_deadly_write_reg(device, 0x0205, postvalues[j+1]);
	}
	// strange commit, but then again, previous writes were strange too
	stk_deadly_write_reg(device, 0x0200, 0x0006); 
	
	// read until we locate a 201 == 0x04
	int response_ok = 0;
	int i;
	for(i=0;i<500;i++) {
		ret = stk_read_registry(device, 0x0201);
		if(ret == 0) {
			printf(".");
			//fsync(stdout);
		} else if(ret == 0x04) {
			printf("\n 0x04 read from 201, we're fine\n");
			response_ok = 1;
			break;
		} else {
			printf("\nRead %02x from 201, we don't know what this means \n", ret);
		}
	}
	if(!response_ok) {
		printf("\nWe read zeroes only !!!, this could be normal, could be not, anyway, stopping it \n");
	}
	return 0;
}
