/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <errno.h>

#include "stktools.h"

void exitm(char *message) {
	if(message != NULL) {
		printf("Exit: %s \n", message);
	}
	exit(1);
}

int main() {
	ut_usb_device *ud;

	printf("Testing if we can reset the dc-1125 on usb\n");

	ud = stk_find_device();
	
	if(ud != NULL) {
		printf("Your Syntek DC-1125 was found, attempting reset\n");
		stk_reset_device(ud);
	} else {
		printf("Your Syntek DC-1125 was not found, ignoring request to reset \n");
	}

	printf("Application Done \n");
	return 0;
}

