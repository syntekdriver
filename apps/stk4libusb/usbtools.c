/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "usbtools.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <dirent.h>
#include <fcntl.h>
#include <unistd.h>

#include <errno.h>

// comment next line to disable debugging
// #define DEBUG_FLAG 

#ifdef DEBUG_FLAG
#define DEBUG(x) x
#else
#define DEBUG(x) 
#endif

int ut_fetch_device_descriptors(ut_usb_device *dev, int fd);


/**
 * Puts the devices in **devices and 
 * returns the count. Returns < 0 on error.
 */
int ut_get_devices(ut_usb_bus **first_bus) {
	ut_usb_device *device = NULL;
	ut_usb_device *temp_device = NULL;
	ut_usb_bus *bus = NULL;
	ut_usb_bus *temp_bus = NULL;
	ut_usb_bus *first = NULL;
	char filename[1024];
	int fd;
	
	int devnum = 0, i=0, busnum = 0;
	int j; int k;
	
	i=0;
	j=0;
	k=0;
	
	DIR *dir;
	struct dirent *entry;

	dir = opendir(UT_USB_PATH);
	
	if(!dir) {
		fprintf(stderr, "Can't open usb_path %s\n", UT_USB_PATH);
		perror("Open usb directory failed");
		return UT_ERROR;
	}
	
	while( (entry = readdir(dir)) != NULL ) {
		// filename checks;
		if(entry->d_name[0] == '.') {
			continue;
		}
		// let's imitate libusb, dirname must end with a number
		char tmp = entry->d_name[strlen(entry->d_name) - 1];
		if(tmp <= 48 || tmp >= 57) {
			DEBUG( printf("Does not look like a bus dir '%s'\n", entry->d_name); );
			continue;
		}
		
		DEBUG( printf("Bus dir '%s'\n", entry->d_name); );
		
		bus = malloc( sizeof(ut_usb_bus) );
		if(first == NULL) {
			first = bus;
		} else {
			temp_bus->next = bus;
		}
		temp_bus = bus;
		
		memset(bus, sizeof(ut_usb_bus), 0);
		strncpy(bus->dirname, entry->d_name, 254);
		bus->dirname[254] = 0; // make sure string ends
		bus->next = NULL;
		bus->devices = NULL;
		
		busnum++;
	}
	
	closedir(dir);
	
	
	
	
	DEBUG( printf("Scanning devices in busses\n"); );
	
	bus = first;
	while( bus != NULL ) {
		char current_path[1024];
		snprintf(current_path, 1023, "%s/%s", UT_USB_PATH, bus->dirname);
		
		dir = opendir(current_path);
		if(!dir) {
			fprintf(stderr, "Can't open usb_path %s\n", current_path);
			perror("Open usb bus directory failed");
			return UT_ERROR;
		}
		
		while( (entry = readdir(dir)) != NULL ) {
			// filename checks;
			if(entry->d_name[0] == '.') {
				continue;
			}
			DEBUG( printf("Found device '%s/%s' \n", current_path, entry->d_name) );
			// handle our device
			snprintf(filename, 1023, "%s/%s", current_path, entry->d_name);
			filename[1024] = 0;
			
			
			fd = open(filename, O_RDWR);
			if (fd < 0) {
				printf("Could not open file '%s' for r/w, skipping \n", filename);
				continue;
			}
			// ok, we have the file open
			struct usbdevfs_connectinfo *connectinfo = NULL;
			connectinfo = malloc(sizeof(struct usbdevfs_connectinfo));
			
			memset(connectinfo, 0, sizeof(struct usbdevfs_connectinfo));
			// connect info 
			int ret;
			
			ret = ioctl(fd, USBDEVFS_CONNECTINFO, connectinfo);
			if (ret < 0) {
				printf("Could not get connection info, skipping \n");
				continue;
			} else {
				//dev->devnum = connectinfo.devnum;
				DEBUG( printf("Connect info %d, %d\n", connectinfo->devnum, (int) connectinfo->slow); );
				//ut_hexdump((unsigned char *) &connectinfo, sizeof(struct usbdevfs_connectinfo), 16);
			}
			
			device = malloc(sizeof(ut_usb_device));
			if(bus->devices == NULL) {
				bus->devices = device;
			} else {
				temp_device->next = device;
			}
			temp_device = device;
			
			memset(device, sizeof(device), 0);
			device->devnum = connectinfo->devnum;
			device->bus = bus;
			device->configs = NULL;
			device->fd = 0;
			memset(device->filename, 0, 1024);
			strncpy(device->filename, filename, 1023);
			
			free(connectinfo);
			
			ut_fetch_device_descriptors(device, fd);
			
			devnum++;
			close(fd);
		}
		
		closedir(dir);
		bus = bus->next;
	}
	
	*first_bus = first;
	return devnum;
}

/* that'd better be one hell of a descriptor */
#define MAX_DESCRIPTOR 128*1024  

int ut_fetch_device_descriptors(ut_usb_device *dev, int fd) {
	char buffy[1024];
	char *b = NULL;
	int read_count=0; int total_read = 0;
	int parse_pos = 0;
	struct usb_descriptor_header *header;
	
	while( (read_count = read(fd, buffy, 1024)) > 0 ) {
		total_read = total_read + read_count;
		b = realloc(b, total_read);
		memcpy(b + (total_read - read_count), buffy, read_count);
		if(read_count < 1024) {
			break;
		}
		if(total_read > MAX_DESCRIPTOR) {
			// memory limit hit
			printf("Error, descriptor memory limit hit");
			return UT_ERROR;
		}
	}
	
	if(total_read < 2) {
		printf("Read less than 2 bytes from file, aborting \n");
		return UT_ERROR;
	}
	
	// ok, b is now filled with data, let's start to parse it
	
	ut_usb_config *last_config = NULL;
	ut_usb_interface *last_interface = NULL;
	ut_usb_endpoint *last_endpoint = NULL;
	
	ut_usb_config *config = NULL;
	ut_usb_interface *interface = NULL;
	ut_usb_endpoint *endpoint = NULL;	

	//printf("Device header \n");
	//ut_hexdump((unsigned char *)b, total_read, 16);
	
	while(parse_pos < total_read) {
		header = (struct usb_descriptor_header *) (b + parse_pos);
		
		parse_pos += header->bLength;
		switch(header->bDescriptorType) {
			case USB_DT_DEVICE:
				// device entry
				//printf("Device found \n");
				dev->descriptor = (struct usb_device_descriptor *) header;
				break;
			case USB_DT_CONFIG:
				// configuration
				config = malloc(sizeof(ut_usb_config));
				if(dev->configs == NULL) {
					dev->configs = config;
				} else {
					last_config->next = config;
				}
				last_config = config; // make us the last
				config->descriptor = (struct usb_config_descriptor *) header;
				config->interfaces = NULL;
				config->next = NULL;
				config->device = dev;
				break;
			case USB_DT_INTERFACE:
				// interface
				//printf("Interface found \n");
				interface = malloc(sizeof(ut_usb_interface));
				if(config->interfaces == NULL) {
					config->interfaces = interface;
				} else {
					last_interface->next = interface;
				}
				last_interface = interface; // make us the last
				interface->descriptor = (struct usb_interface_descriptor *) header;
				interface->endpoints = NULL;
				interface->next = NULL;
				interface->config = config;
				break;
			case USB_DT_ENDPOINT:
				// endpoint
				//printf("Endpoint found \n");
				endpoint = malloc(sizeof(ut_usb_endpoint));
				if(interface->endpoints == NULL) {
					interface->endpoints = endpoint;
				} else {
					last_endpoint->next = endpoint;
				}
				last_endpoint = endpoint; // make us the last
				endpoint->descriptor = (struct usb_endpoint_descriptor *) header;
				endpoint->next = NULL;
				endpoint->interface = interface;
				break;
				break;
			default:
				// ignore
				//printf("Ignoring descriptor type %d \n", header->bDescriptorType);
				break;
		}
	}
	
	return UT_OK;
}



/**
 * Open device
 */
int ut_open_device(ut_usb_device *device) {
	int fd;
	if(strlen(device->filename) < 1) {
		printf("Device filename has not been initialized\n");
		return UT_ERROR;
	}
	if(device->fd > 0) {
		printf("Device is already open\n");
		return UT_ERROR;
	}
	device->fd = 0;
	fd = open(device->filename, O_RDWR);
	if (fd < 0) {
		printf("Could not open file '%s' for r/w \n", device->filename);
		return UT_ERROR;
	}
	device->fd = fd;
	return UT_OK;
}

/**
 * Close device
 */
int ut_close_device(ut_usb_device *device) {
	if(device->fd > 0) {
		if(close(device->fd) == 0) {
			device->fd = 0;
			return UT_OK;
		} else {
			printf("Device is open but close returned nonzero \n");
		}
	} else {
		printf("Device not open \n");
	}
	return UT_ERROR;
}

int ut_reset_device(ut_usb_device *device) {
	int ret;
	ret = ioctl(device->fd, USBDEVFS_RESET, NULL);
	if (ret < 0) {
		return UT_ERROR;
	}
	return UT_OK;
}

int ut_control_msg(
	ut_usb_device *device, int requesttype, int request, 
	int value, int index, char *bytes, int size, int timeout
) {
	int ret;
	/* cross your fingers */
	struct usbdevfs_ctrltransfer ctrl;
	
	ctrl.bRequestType = requesttype;
	ctrl.bRequest = request;
	ctrl.wValue = value;
	ctrl.wIndex = index;
	ctrl.wLength = size;
	ctrl.timeout = timeout;
	ctrl.data = (void *) bytes;
	
	//ut_hexdump((unsigned char *) &ctrl, sizeof(struct usbdevfs_ctrltransfer) , 16);
	
	ret = ioctl(device->fd, USBDEVFS_CONTROL, &ctrl);

	if(ret < 0) {
		return UT_ERROR;
	}
	
	return ret;
}



/**
 * Claim the interface, the device is found out 
 * accordingly to the interface
 */
int ut_claim_interface(ut_usb_interface *interface) {
	int ret;
	ut_usb_device *device;
	unsigned int iNumber = interface->descriptor->bInterfaceNumber;
	device = interface->config->device;
	
	printf("Claiming interface %u \n", iNumber);
	
	ret = ioctl(device->fd, USBDEVFS_CLAIMINTERFACE, &iNumber);
	
	if(ret < 0) {
		printf("CLAIMINTERFACE failed \n");
		if(errno == EBUSY) {
			printf("CLAIMINTERFACE returned EBUSY, make sure you have proper access to device \n");
		}
		return UT_ERROR;
	}
	
	return UT_OK;
}

/**
 * Claim the interface, the device is found out 
 * accordingly to the interface
 */
int ut_release_interface(ut_usb_interface *interface) {
	int ret;
	ut_usb_device *device;
	unsigned int iNumber = interface->descriptor->bInterfaceNumber;
	device = interface->config->device;
	
	ret = ioctl(device->fd, USBDEVFS_RELEASEINTERFACE, &iNumber);
	
	if(ret < 0) {
		printf("RELEASEINTERFACE failed \n");
		return UT_ERROR;
	}
	return UT_OK;
}

int ut_set_interface(ut_usb_interface *interface) {
	int ret;
	struct usbdevfs_setinterface us;
	ut_usb_device *device;
	us.interface = interface->descriptor->bInterfaceNumber;
	us.altsetting = interface->descriptor->bAlternateSetting;
	
	device = interface->config->device;
	
	ret = ioctl(device->fd, USBDEVFS_SETINTERFACE, &us);
	
	if(ret < 0) {
		printf("SETINTERFACE failed \n");
		return UT_ERROR;
	}
	return UT_OK;
}

int ut_set_configuration(ut_usb_config *config) {
	int ret;
	ut_usb_device *device;
	unsigned int cNumber = config->descriptor->bConfigurationValue;
	device = config->device;
	
	ret = ioctl(device->fd, USBDEVFS_SETCONFIGURATION, &cNumber);
	
	if(ret < 0) {
		printf("SETCONFIGURATION failed \n");
		return UT_ERROR;
	}
	return UT_OK;
}

void ut_dump_urb(struct usbdevfs_urb *urb) {
	printf("urb dump\n");
	printf("Type: %d \n", urb->type);
	printf("Endpoint: %d \n", urb->endpoint);
	printf("Status: %d \n", urb->status);	
	printf("Flags: %d \n", urb->flags);
	printf("Buffer: %04x \n", (int) urb->buffer);
	printf("BufferLength: %d \n", urb->buffer_length);
	printf("ActualLength: %d \n", urb->actual_length);
	printf("StartFrame: %d \n", urb->start_frame);
	printf("NumberOfPackets: %d \n", urb->number_of_packets);
	printf("ErrorCount: %d \n", urb->error_count);
	printf("SigNr %u \n", urb->signr);  /* signal to be sent on error, -1 if none should be sent */
	//void *usercontext;
	printf("------------\n");
}

/**
 * TODO : check if it's working correctly
 * consider a static urb struct to avoid malloc stress
 */ 
int ut_submit_iso_read(ut_usb_endpoint *ep, unsigned char *buffer, 
	int packet_size, int packet_count, int error_signal, struct usbdevfs_urb *template) {
	
	int i;
	int ret;
	struct usbdevfs_urb *urb;
	int urbsize = sizeof(struct usbdevfs_urb) + ( (packet_count) * sizeof(struct usbdevfs_iso_packet_desc) );
	
	if(template == NULL) {
		urb = malloc(urbsize);
		
		memset(urb, 0, urbsize);
		
		urb->type = USBDEVFS_URB_TYPE_ISO;
		urb->endpoint = ep->descriptor->bEndpointAddress | USB_DIR_IN;
		urb->flags = USBDEVFS_URB_ISO_ASAP;
		urb->signr = error_signal;
		urb->start_frame = 0;
	} else {
		// ???
		urb = template;
		// fast cleanup
		memset(buffer, 0, packet_size * packet_count);
	}
	
	urb->buffer = buffer;
	urb->buffer_length = packet_size * packet_count;
	urb->number_of_packets = packet_count;
	
	for(i=0;i<packet_count;i++) {
		urb->iso_frame_desc[i].length = packet_size;
	}
	ut_usb_device *device = ep->interface->config->device;
	
	//ut_hexdump((unsigned char *) urb, urbsize, 16);
	//ut_dump_urb(urb);

	ret = ioctl(device->fd, USBDEVFS_SUBMITURB, urb);
	if(ret < 0) {
		perror("ioctl failed to submiturb");
	}
	
	return ret;
}

/**
 * TODO triplecheck this.
 */
int ut_catch_iso_read(ut_usb_endpoint *ep, struct usbdevfs_urb **ref) {
	int ret;
	struct usbdevfs_urb *urb = NULL;
	ut_usb_device *device = ep->interface->config->device;
	
	*ref = NULL;
	
	ret = ioctl(device->fd, USBDEVFS_REAPURB , &urb);
	if(ret < 0) {
		printf("ioctl failed in iso catch");
		return UT_ERROR;
	}
	if(urb->type == USBDEVFS_URB_TYPE_ISO) {
		//printf("Caught ISO response\n");
	} else {
		printf("Got weird urb with type %d\n", urb->type);
		*ref = NULL;
		return -1;
	}
	*ref = urb;
	return UT_OK;
}

int ut_reset_endpoint(ut_usb_endpoint *ep) {
	int ret;
	ut_usb_device *device;

	device = ep->interface->config->device;
	
	unsigned int epAddress = ep->descriptor->bEndpointAddress;
	
	// epAddress = epAddress & USB_ENDPOINT_NUMBER_MASK; // are we supposed to do this ?
	
	ret = ioctl(device->fd, USBDEVFS_RESETEP, &epAddress);
	
	if(ret < 0) {
		perror("Failed to reset ep");
		printf("RESET Endpoint failed \n");
		return UT_ERROR;
	} else {
		printf("endpoint was reset at %02x \n", epAddress);
	}
	return UT_OK;
}

void ut_hexdump(unsigned char *data, int len, int cols) {
	int i = 0;
	while(i < len) {
		printf("%02x ", (unsigned int)data[i]);
		i++;
		if(i%cols == 0) {
			printf("\n");
		}
	}
	printf("\n");
}



