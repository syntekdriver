/**
 *  All these sources are provided under the BSD Licence 
 *  that can be found in the bsd_licence.txt within the same
 *  directory.
 *
 */

#ifndef STKTOOLS_H
#define STKTOOLS_H

#include "usbtools.h"
#include <linux/usb.h> // some of our stuff comes from there

#define SYNTEK_ERROR -1
#define SYNTEK_OK 0

#define SYNTEK_VENDOR_ID 0x174f
#define SYNTEK_DC1125_ID 0xa311


/* finds the device */
ut_usb_device *stk_find_device();

int stk_get_info(ut_usb_device *dev);

int stk_reset_device(ut_usb_device *dev);
int stk_set_feature(ut_usb_device *device, int index);

int stk_write_registry(ut_usb_device *device, int value, int index);
int stk_read_registry(ut_usb_device *device, int index);

int stk_deadly_write_reg(ut_usb_device *device, int index, int value);
int stk_initialization_first1(ut_usb_device *device);
int stk_initialization_first2(ut_usb_device *device);
int stk_initialization1(ut_usb_device *device);
int stk_initialization2(ut_usb_device *device);
int subblock_omega(ut_usb_device *device);
int subblock_alpha(ut_usb_device *device);



#endif /* STKTOOLS_H */

