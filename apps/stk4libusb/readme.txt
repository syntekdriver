 
 
--------------------------------------------------
 
 [The Readme, Handle this stuff with CARE ! Experimental and maybe dangerous software on your hands]
 
This folder contains the userland applications for testing the behaviour of syntek dc-1125 camera chip
 
Use make <target> to create specific applications
 
Example:
	make test_stream
	
Targets:
	
	test_stream - video capturer /* work in progress, blame yourself if you crash */
	
	
	reset_device - resets the device and ALL DEVICES on the same bus, handle with care !
	
	
Running:

	to capture a full blown log of iso transfers in more human readable format:
	./test_stream
	
	to capture raw video data (no extra data, even isoc frame magic markers are removed)
	./test_stream video
	
	the outputfile will be logfile_<timestamp>.txt or logfile_<timestamp>.data accordingly to mode

	The program will do 5000 isoc transfers (should take a few seconds) and then stops by itself.
	
	Please don't press ctrl+c to stop it unless you get fatal error messages or hanging, otherwise
	you may disrupt our sequences and we can't promise any kind of stability after that at all.
	
	The binary movie data seems to "repeat the data" after every 640 bytes ... so my 2 cents are
	on raw bayer (the img resolution is 640x480), let's hope i'm right (read this as we may be 
	saved).
	
	
You can also experiment yourself here in this directory, if you create anything useful, make sure you let us know.

NB! All the stuff is currently being developed on x86-32 bit linux, anomalities on other platforms possible
PS. You probably need to be root to run this stuff.
